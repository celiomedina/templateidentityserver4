# IdentityServer4 
Implementação do IdentityServer4 com o MongoDB


Nota: Pequena parte da persistência no MongoDB.

A solução é composta por:

IdentityServer - projeto baseado no IdentityServer4 que gerencia a autenticação
ControlIdentityServer - é um projeto de API que tem implantado a autenticação no servidor do IdentityServer e também a responsável 
pelo cadastro dde Usuários.
clients / Api - é um porjeto API que tem como finalidade acessar o ControlIdentityServer passando dados de login com usuário e senha e ClientId.
No cliente foi implantado o uso do Swashbuckle. 
A solução é baseada no Visual Studio 2019.
ASP .Net Core 3.1

Nugets mais relevantes:
MongoDB.Driver
Swashbuckle

para rodar o projeto todo funcionar, será necessário:
1 - Criar um servidor MongoDB
2 - Alterar o AppSettings do projeto IdentityServer e ControlIdentityServer com a conexão do MongoDB criado no passo anterior
3 - Executar o projeto IdentityServer (Na pasta do projeto, rodar o comando "dotnet run")
4 - Executar o projeto ControlIdentityServer
5 - Executar o projeto ClienteApi

No VisualStudio tem a opção de execução de múltiplos projetos clicando com o lado direito do mouse na Solution e selecinando e ordenando a execuçaõ dos projetos.

Na primeira execução, com o intuito de acelerar os testes, na Startup do projeto IdentityServer tem um Inicilizador do MongoDB onde registros iniciais serão criados.

Será necessário reiniciar a execuação do pojeto IdentityServer.
﻿
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AutenticacaoDeUsuario.Controllers.Models;
using AutenticacaoDeUsuario.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AutenticacaoDeUsuario.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    public class UsuarioController : Controller
    {

        private readonly IOptions<ConfigurationOptions> _settings;

        public UsuarioController(IOptions<ConfigurationOptions> settings)
        {
            _settings = settings;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///       "subjectId": "1",
        ///       "username": "username",
        ///       "password": "password",
        ///       "isActive": true
        ///     }
        ///
        /// </remarks>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<ObjectResult> Create([FromBody]UserRegisterModel model, string token)
        {
            string url = $"{_settings.Value.IdentityServerUrl}/user/create";

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var client3 = new HttpClient();
            if (token != null) client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var apiResponse = client3.PostAsync(url, stringContent).Result;

            return StatusCode((int)apiResponse.StatusCode, apiResponse.ReasonPhrase);
        }
    }
}

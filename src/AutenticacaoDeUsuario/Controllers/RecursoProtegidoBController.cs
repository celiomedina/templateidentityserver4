﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using IdentityModel.Client;
using static IdentityModel.OidcConstants;
using System.Text;
using AutenticacaoDeUsuario.Model;
using Microsoft.Extensions.Options;

namespace AutenticacaoDeUsuario.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    [Microsoft.AspNetCore.Authorization.Authorize]
    public class RecursoProtegidoBController : Controller
    {
        private readonly IOptions<ConfigurationOptions> _settings;

        public RecursoProtegidoBController(IOptions<ConfigurationOptions> settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Login no recurso protegido B que dá apenas acesso ao recurso B
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        [HttpGet("LoginB")]
        public async Task<string> LoginAsync(string user, string pwd)
        {
            StringBuilder result = new StringBuilder();
            var IdentityServerClient = new HttpClient();
            var discoveryData = await IdentityServerClient.GetDiscoveryDocumentAsync(_settings.Value.IdentityServerUrl);
            if (discoveryData.IsError)
            {
                Console.WriteLine(discoveryData.Error);
                return "";
            }

            string usuario = user;
            string senha = pwd;

            var responseAutenticacaoIdentityServer = await IdentityServerClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = discoveryData.TokenEndpoint,
                GrantType = GrantTypes.Password,
                ClientId = "AutenticacaoDeUsuario",
                ClientSecret = "secret",
                Scope = "ApiRecursoProtegidoB",

                UserName = usuario,
                Password = senha
            });

            if (!responseAutenticacaoIdentityServer.IsError)
            {
                result.Append($"JWT{Environment.NewLine}{Environment.NewLine}");
                result.Append(responseAutenticacaoIdentityServer.AccessToken);

                result.Append($"{Environment.NewLine}{Environment.NewLine}JSON{Environment.NewLine}{Environment.NewLine}");
                result.Append(responseAutenticacaoIdentityServer.Json);
            }
            else
            {
                result.Append(responseAutenticacaoIdentityServer.Error);
            }

            return result.ToString();
        }


        /// <summary>
        /// Recurso protegido - Consulta
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("ConsultaRecursoProtegidoB")]
        public async Task<IActionResult> ConsultaRecursoProtegido(string token)
        {
            string url = $"{_settings.Value.ApiRecursoProtegidoB}/RecursoProtegido";

            var client3 = new HttpClient();
            if (token != null) client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var apiResponse = client3.GetAsync(url).Result;

            return StatusCode((int)apiResponse.StatusCode, apiResponse.ReasonPhrase);
        }
    }
}
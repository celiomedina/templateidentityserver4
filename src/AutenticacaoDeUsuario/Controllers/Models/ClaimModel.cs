﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutenticacaoDeUsuario.Models
{
    public class ClaimModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}

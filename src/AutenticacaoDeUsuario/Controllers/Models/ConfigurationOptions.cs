﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutenticacaoDeUsuario.Model
{
    public class ConfigurationOptions
    {
        public string IdentityServerUrl { get; set; }
        public string ApiRecursoProtegidoA { get; set; }
        public string ApiRecursoProtegidoB { get; set; }
    }
}

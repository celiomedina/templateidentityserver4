﻿using AutenticacaoDeUsuario.Models;
using System.Collections.Generic;

namespace AutenticacaoDeUsuario.Controllers.Models
{
    public class UserRegisterModel
    {
        public UserRegisterModel() { }

        public string SubjectId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public List<ClaimModel> Claims { get; set; }
    }
}

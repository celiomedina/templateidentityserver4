﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AutenticacaoDeUsuario.Model;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using static IdentityModel.OidcConstants;

namespace AutenticacaoDeUsuario.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    public class RecursoProtegidoAController : Controller
    {
        private readonly IOptions<ConfigurationOptions> _settings;

        public RecursoProtegidoAController(IOptions<ConfigurationOptions> settings) 
        {
            _settings = settings;
        }

        /// <summary>
        /// Login no Recurso protegido A que dá acesso aos 2 sistemas.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        [HttpGet("Login")]
        public async Task<IActionResult> LoginAsync(string user, string pwd)
        {
            StringBuilder result = new StringBuilder();
            var IdentityServerClient = new HttpClient();
            var discoveryData = await IdentityServerClient.GetDiscoveryDocumentAsync(_settings.Value.IdentityServerUrl);
            if (discoveryData.IsError)
            {
                return StatusCode((int)discoveryData.HttpStatusCode, discoveryData.Error);
            }

            var responseAutenticacaoIdentityServer = await IdentityServerClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = discoveryData.TokenEndpoint,
                GrantType = GrantTypes.Password,
                ClientId = "AutenticacaoDeUsuario",
                ClientSecret = "secret",
                Scope = "ApiRecursoProtegidoA",

                UserName = user,
                Password = pwd
            });

            if (!responseAutenticacaoIdentityServer.IsError)
                return Ok(responseAutenticacaoIdentityServer.AccessToken);

            return BadRequest(responseAutenticacaoIdentityServer.Error);
        }

        /// <summary>
        /// Recurso Protegido - Consulta
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("ConsultaRecursoProtegidoA")]
        public async Task<IActionResult> ConsultaRecursoProtegido(string token)
        {
            string url = $"{_settings.Value.ApiRecursoProtegidoA}/RecursoProtegido";

            var client3 = new HttpClient();
            if (token != null) client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var apiResponse = client3.GetAsync(url).Result;

           return StatusCode((int)apiResponse.StatusCode, apiResponse.ReasonPhrase);
        }
    }
}
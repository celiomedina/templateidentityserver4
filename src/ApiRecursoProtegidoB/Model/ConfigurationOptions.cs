﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRecursoProtegidoB.Model
{
    public class ConfigurationOptions
    {
        public string IdentityServerUrl { get; set; }
    }
}

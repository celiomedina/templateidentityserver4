using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace ApiRecursoProtegidoB.Controllers
{
    [Route("RecursoProtegido")]
    [Authorize]
    public class RecursoProtegidoController : ControllerBase
    {
        public IActionResult Get()
        {
            return Ok(new JsonResult("Sucesso!!"));
        }
    }
}
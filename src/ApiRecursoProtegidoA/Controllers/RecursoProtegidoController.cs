﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiRecursoProtegidoA.Controllers
{
    [Route("RecursoProtegido")]
    [Authorize("Founder")]
    public class RecursoProtegidoController : ControllerBase
    {
        public IActionResult Get()
        {
            return Ok(new JsonResult("Sucesso!!"));
        }
    }
}
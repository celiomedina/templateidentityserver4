﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class ClaimModel
    {
        public ClaimModel() { }
        public string Type { get; set; }
        public string Value { get; set; }

    }
}

﻿
using System.Linq;
using System.Threading.Tasks;
using IdentityServer.Infrastructure.Repositories.Interfaces;
using IdentityServer.Interfaces;
using IdentityServer.Models;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace IdentityServer.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("user")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserClaimRepository _userClaimRepository;
        public UserController(IUserRepository userRepository, IUserClaimRepository userClaimRepository)
        {
            _userRepository = userRepository;
            _userClaimRepository = userClaimRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///       "subjectId": "1",
        ///       "username": "username",
        ///       "password": "password",
        ///       "isActive": true,
        ///       "claims": [
        ///         {
        ///             "type": "SIGSA",
        ///             "value": "USUARIO.EXCLUIR"
        ///         }
        ///       ]
        ///     }
        ///
        /// </remarks>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<ActionResult> Create([FromBody]UserRegisterModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid && model != null)
            {
                var user = new ApplicationUser(model.Username)
                {
                    Username = model.Username,                    
                    IsActive = model.IsActive,
                    Password = model.Password.Sha256(),
                    SubjectId = model.SubjectId                    
                };
                _userRepository.Add(user);
                var userSaved = _userRepository.Where(c => c.Username == user.Username).FirstOrDefault();
                var userClaims = model.Claims.Select(
                    c => new UserClaim() {
                        UserId = userSaved.Id,
                        ClaimType = c.Type,
                        ClaimValue = c.Value
                    });

                _userClaimRepository.Add(userClaims);

                return Ok();
            }

            return BadRequest();
        }
    }
}
﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson.Serialization;
using System;
using IdentityServer.Interfaces;
using IdentityServer.Options;
using IdentityServer.Extension;
using IdentityServer4.Test;
using System.Security.Cryptography.X509Certificates;
using IdentityServer.Models;
using IdentityServer.Services;
using Microsoft.AspNetCore.Authorization;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using IdentityServer.Infrastructure.Repositories.Interfaces;
using AspNet.Security.OAuth.Validation;

namespace IdentityServer
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            var environmentVar = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (environmentVar == null)
            {
                environmentVar = env.EnvironmentName;
            }
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environmentVar}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
           

            // Dependency Injection - Register the IConfigurationRoot instance mapping to our "ConfigurationOptions" class 
            services.Configure<ConfigurationOptions>(Configuration);

            // ---  Configiguando o IdentityServer com o MOngo REspository stores, keys, clients and scopes ---
            services.AddAuthentication(OAuthValidationDefaults.AuthenticationScheme).AddOAuthValidation();
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidatorService>()
                .AddProfileService<ProfileService>()
                .AddJwtBearerClientAuthentication()
                .AddRepositories()
                .AddServices();

           

            #region Adicionando OpenId Connect
            //Adicionando o OpenId Connect
            //services.AddAuthentication()
            //  .AddOpenIdConnect("oidc", "OpenID Connect", options =>
            //  {
            //      options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
            //      options.SignOutScheme = IdentityServerConstants.SignoutScheme;

            //      options.Authority = "https://demo.identityserver.io/";
            //      options.ClientId = "cliente_api";

            //      options.TokenValidationParameters = new TokenValidationParameters
            //      {
            //          NameClaimType = "name",
            //          RoleClaimType = "role"
            //      };
            //  });
            #endregion

        }

        [Obsolete]
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            
            app.UseIdentityServer();
            //app.UseAuthentication();
            app.UseStaticFiles();
            app.UseRouting();
            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            // --- Configure Classes to ignore Extra Elements (e.g. _Id) when deserializing ---
            ConfigureMongoDriver2IgnoreExtraElements();

            // --- The following will do the initial DB population (If needed / first time) ---
            InitializeDatabase(app);
        }

        #region Database
        private static void InitializeDatabase(IApplicationBuilder app)
        {
            bool createdNewRepository = false;
            var repositoryClient = app.ApplicationServices.GetService<IClientRepository>();
            var repositoryIdentityResource = app.ApplicationServices.GetService<IIdentityResourceRepository>();
            var repositoryApiResource = app.ApplicationServices.GetService<IApiResourceRepository>();
            var repositoryApplicationUser = app.ApplicationServices.GetService<IUserRepository>();
            var repositoryPersistedGrant = app.ApplicationServices.GetService<IPersistedGrantRepository>();
            var repositoryIdentityUserClaim = app.ApplicationServices.GetService<IUserClaimRepository>();

            //  --Client
            if (!repositoryClient.CollectionExists())
            {
                foreach (var client in IdentityServerDBSeed.GetClients())
                {
                    repositoryClient.Add(client);
                }
                createdNewRepository = true;
            }

            //  --IdentityResource
            if (!repositoryIdentityResource.CollectionExists())
            {
                foreach (var res in IdentityServerDBSeed.GetIdentityResources())
                {
                    repositoryIdentityResource.Add(res);
                }
                createdNewRepository = true;
            }
            //  --ApiResource
            if (!repositoryApiResource.CollectionExists())
            {
                foreach (var api in IdentityServerDBSeed.GetApiResources())
                {
                    repositoryApiResource.Add(api);
                }
                createdNewRepository = true;
            }

            //int maxId = 0;
            //maxId = repositoryIdentityUserClaim.Where(c => c.Id.m)
            //  --Usuários
            if (!repositoryApplicationUser.CollectionExists())
            {
                foreach (var api in IdentityServerDBSeed.GetUsers())
                {
                    repositoryApplicationUser.Add(api);
                    var user = repositoryApplicationUser.Where(c => c.Username == api.Username).FirstOrDefault();
                    repositoryIdentityUserClaim.Add(IdentityServerDBSeed.GetIdentityClaims(user.Id));
                }
                createdNewRepository = true;
            }

            // If it's a new Repository (database), need to restart the website to configure Mongo to ignore Extra Elements.
            if (createdNewRepository)
            {
                var newRepositoryMsg = $"Mongo Repository created/populated! Please restart you website, so Mongo driver will be configured  to ignore Extra Elements - e.g. IdentityServer \"_id\" ";
                //throw new Exception(newRepositoryMsg);
            }
        }

        /// <summary>
        /// Configure Classes to ignore Extra Elements (e.g. _Id) when deserializing
        /// As we are using "IdentityServer4.Models" we cannot add something like "[BsonIgnore]"
        /// </summary>
        private static void ConfigureMongoDriver2IgnoreExtraElements()
        {
            BsonClassMap.RegisterClassMap<Client>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<IdentityResource>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<ApiResource>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<Resource>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<PersistedGrant>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<ApplicationUser>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<UserClaim>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });

        }

        #endregion

    }
}

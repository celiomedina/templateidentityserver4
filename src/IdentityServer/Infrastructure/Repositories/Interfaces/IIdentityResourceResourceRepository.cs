﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IdentityServer.Interfaces
{
    public interface IIdentityResourceRepository : IRepositoryBase<IdentityResource>
    {

    }
}

﻿using IdentityServer.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IdentityServer.Interfaces
{
    public interface IUserRepository: IRepositoryBase<ApplicationUser>
    {
        ApplicationUser FindBySubjectId(string subjectId);
    }
}

﻿using IdentityServer.Interfaces;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Infrastructure.Repositories.Interfaces
{
    public interface IUserClaimRepository : IRepositoryBase<UserClaim>
    {
    }
}

﻿using IdentityServer.Infrastructure;
using IdentityServer.Interfaces;
using IdentityServer4.Models;
using IdentityServer.Options;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace IdentityServer.Repositories
{
    public class ApiResourceRepository : RepositoryBase<ApiResource>, IApiResourceRepository
    {
        //public ApiResourceRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase)
        //{

        //}

        public ApiResourceRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {

        }
    }

}

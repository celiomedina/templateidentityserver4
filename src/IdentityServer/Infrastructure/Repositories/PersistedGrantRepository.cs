﻿using IdentityServer.Interfaces;
using IdentityServer.Options;
using IdentityServer4.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Infrastructure.Repositories
{
    public class PersistedGrantRepository : RepositoryBase<PersistedGrant>, IPersistedGrantRepository
    {
        //public PersistedGrantRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase)
        //{

        //}

        public PersistedGrantRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {

        }
    }
}

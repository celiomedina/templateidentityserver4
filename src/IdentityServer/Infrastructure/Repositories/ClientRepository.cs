﻿using IdentityServer.Infrastructure;
using IdentityServer4.Models;
using IdentityServer.Options;
using Microsoft.Extensions.Options;

namespace IdentityServer.Repositories
{
    public class ClientRepository : RepositoryBase<Client>, Interfaces.IClientRepository
    {
        //public ClientRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase)
        //{

        //}

        public ClientRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {

        }
    }
}

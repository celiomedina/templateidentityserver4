﻿using IdentityServer.Models;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace IdentityServer.Configuration
{
    public class IdentityServerDBSeed
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResource("roles", new[] { "role" })
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {               
                new ApiResource("ApiRecursoProtegidoA", "Api Recurso Protegido A"),
                new ApiResource("ApiRecursoProtegidoB", "Api Recurso Protegido B")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {                
                new Client
                {
                    ClientId = "AutenticacaoDeUsuario",
                    AllowedGrantTypes  =  GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    AllowOfflineAccess = true,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {
                        "ApiRecursoProtegidoA",
                        "ApiRecursoProtegidoB",
                        IdentityServerConstants.StandardScopes.OpenId,
                       IdentityServerConstants.StandardScopes.Profile,
                       "roles"
                    },
                    //Claims = new List<Claim>()
                    //{
                    //    new Claim(ClaimTypes.Role, "Administrator")
                    //},
                    AccessTokenType = AccessTokenType.Jwt
                }               
            };
        }

        public static List<ApplicationUser> GetUsers()
        {
            return new List<ApplicationUser>
            {
                new ApplicationUser("Admin")
                {
                    SubjectId = "1",
                    Username = "Admin",
                    Password = "1234".Sha256()
                },
                new ApplicationUser("bob")
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "1234".Sha256()
                }
            };
        }

        public static List<UserClaim> GetIdentityClaims(string idUser)
        {
            return new List<UserClaim>
            {
                new UserClaim
                {
                    UserId = idUser,
                    ClaimType = "role", 
                    ClaimValue = "Administrator"
                }
            };
        }
    }
}
﻿using IdentityServer.Options;
using IdentityServer4.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using IdentityServer.Models;

namespace IdentityServer.Infrastructure
{
    public class IdentityServerDBContext
    {
        public readonly IMongoDatabase _database = null;

        public IdentityServerDBContext(IOptions<ConfigurationOptions> settings)
        {
            var client = new MongoClient(settings.Value.MongoConnection);
            if (client != null)
                _database = client.GetDatabase(settings.Value.MongoDatabaseName);
        }

        public IMongoCollection<ApplicationUser> ApplicationUser
        {
            get
            {
                return _database.GetCollection<ApplicationUser>("ApplicationUser");
            }
        }

        public IMongoCollection<ApiResource> ApiResource
        {
            get
            {
                return _database.GetCollection<ApiResource>("ApiResource");
            }
        }

        public IMongoCollection<Client> Client
        {
            get
            {
                return _database.GetCollection<Client>("Client");
            }
        }

        public IMongoCollection<IdentityResource> IdentityResource
        {
            get
            {
                return _database.GetCollection<IdentityResource>("IdentityResource");
            }
        }

        public IMongoCollection<PersistedGrant> PersistedGrant
        {
            get
            {
                return _database.GetCollection<PersistedGrant>("PersistedGrant");
            }
        }
    }
}

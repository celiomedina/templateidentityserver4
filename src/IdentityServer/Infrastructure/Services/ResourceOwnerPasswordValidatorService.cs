﻿using IdentityModel;
using IdentityServer.Interfaces;
using IdentityServer.Models;
using IdentityServer4.Models;
using IdentityServer4.Test;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public class ResourceOwnerPasswordValidatorService : IResourceOwnerPasswordValidator
    {
        private readonly ILoginService<ApplicationUser> _loginService;

        public ResourceOwnerPasswordValidatorService(ILoginService<ApplicationUser> loginService)
        {
            _loginService = loginService;
        }

        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var result = _loginService.ValidateCredentials(context.UserName, context.Password.Sha256()).Result;
            if (result) 
            {
                var user = _loginService.FindByUsername(context.UserName).Result;
                context.Result = new GrantValidationResult(context.UserName, "password", null, "local", null);

                return Task.FromResult(new GrantValidationResult(context.UserName, "password", null, "local", null));
            }

            context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "The username and password do not match", null);
            return Task.FromResult(context.Result);
        }
    }
}
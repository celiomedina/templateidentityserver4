﻿using IdentityServer.Infrastructure.Repositories.Interfaces;
using IdentityServer.Interfaces;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Test;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public class ProfileService : IProfileService
    {
        protected readonly ILogger Logger;


        protected readonly IUserRepository _userStore;
        protected readonly IUserClaimRepository _userIUserClaim;

        public ProfileService(IUserRepository userStore, ILogger<ProfileService> logger, IUserClaimRepository userIUserClaim)
        {
            _userStore = userStore;
            Logger = logger;
            _userIUserClaim = userIUserClaim;
        }


        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();

            Logger.LogDebug("Profile {subject} do client {client} ClamTypes {claimTypes} via {caller}",
                context.Subject.GetSubjectId(),
                context.Client.ClientName ?? context.Client.ClientId,
                context.RequestedClaimTypes,
                context.Caller);

            var user = _userStore.Where(c => c.Username == context.Subject.GetSubjectId()).FirstOrDefault();
            var userClaims = _userIUserClaim.Where(c => c.UserId == user.Id);
            //var claims = new List<Claim>
            //{
            //    new Claim("role", "IdentityServerControl.Administrator"),
            //    new Claim(ClaimTypes.Role, "role"),
            //    new Claim("username", user.Username)
            //};

            context.IssuedClaims = userClaims.Select(c => new Claim(c.ClaimType, c.ClaimValue)).ToList();
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = _userStore.Where(c => c.Username == context.Subject.GetSubjectId()).FirstOrDefault();
            context.IsActive = user != null;
        }
    }
}

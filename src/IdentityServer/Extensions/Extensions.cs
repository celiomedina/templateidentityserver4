﻿using IdentityServer4.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Determina se o cliente está habilitado para uso do PKCE
        /// recurso (definido no RFC 7636) que inclui segurança ao executar o fluxo de códigos de autorização em um dispositivo móvel. 
        /// Ele aborda um possível problema de segurança que pode ocorrer quando as seguintes condições são verdadeiras:
        /// 1 - Não há nenhum segredo do cliente.
        /// 2 - O navegador ou sistema operacional está sendo usado para executar a solicitação de autenticação.
        /// 3 - Um aplicativo móvel nativo está consumindo o redirecionamento da solicitação de autenticação e executando uma troca de código para tokens de acesso no terminal do token.
        /// </summary>
        /// <param name="store">The store.</param>
        /// <param name="client_id">The client identifier.</param>
        /// <returns></returns>
        public static async Task<bool> IsPkceClientAsync(this IClientStore store, string client_id)
        {
            if (!string.IsNullOrWhiteSpace(client_id))
            {
                var client = await store.FindEnabledClientByIdAsync(client_id);
                return client?.RequirePkce == true;
            }

            return false;
        }
    }
}

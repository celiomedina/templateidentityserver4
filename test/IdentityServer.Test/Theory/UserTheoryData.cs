﻿using Castle.Components.DictionaryAdapter;
using IdentityServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Xunit;

namespace IdentityServer.Test.Theory
{
    public class UserTheoryData: TheoryData<ApplicationUser>
    {
        private string UserName { get; set; }
        public UserTheoryData()
        {
            /**
             * Each item you add to the TheoryData collection will try to pass your unit test's one by one.
             */
            UserName = RandomString(10);
            // mock data created by https://barisates.github.io/pretend
            Add(new ApplicationUser(UserName)
            {
                Email = new Range(0, 6).ToString(),
                SubjectId = "1",
                Username = UserName,
                Id = Guid.NewGuid().ToString(),
                Password = "1234",
                IsActive = true
            });
        }

        public UserRegisterModel GetUserRegisterModel()
        {
            return new UserRegisterModel()
            {  
                SubjectId = "1",
                Username = UserName,
                Password = "1234",
                IsActive = true,
                Claims = new List<ClaimModel>()
                {
                    new ClaimModel()
                    {
                        Type = "Test.Role",
                        Value = "Test.Value"
                    }
                }                
            };
        }

        public ApplicationUser GetApplicationUser()
        {
            Random rnd = new Random();
            return new ApplicationUser(UserName)
            {
                Email = UserName,
                SubjectId = "1",
                Username = UserName,
                Id = Guid.NewGuid().ToString(),
                Password = "1234",
                IsActive = true
            };
        }

        private Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}

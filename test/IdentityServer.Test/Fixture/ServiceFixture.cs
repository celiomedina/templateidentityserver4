﻿using IdentityServer.Options;
using IdentityServer.Repositories;
using IdentityServer.Services;
using IdentityServer.Test.Theory;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityServer.Test.Fixture
{
    public class ServiceFixture : IDisposable
    {
        public UserRepository testUserRepository;
        public LoginService testLoginService;

        public ServiceFixture()
        {
            ConfigurationOptions app = new ConfigurationOptions() { MongoConnection = "mongodb://localhost:27017", MongoDatabaseName = "IdentityServerService" };
            var mock = new Mock<IOptions<ConfigurationOptions>>();
            mock.Setup(ap => ap.Value).Returns(app);

            testUserRepository = new UserRepository(mock.Object);

            testUserRepository.Add(new UserTheoryData().GetApplicationUser());

            testLoginService = new LoginService(testUserRepository);
        }

            // https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063?view=vs-2019
            #region ImplementIDisposableCorrectly
            public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't
        // own unmanaged resources, but leave the other methods
        // exactly as they are.
        ~ServiceFixture()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (testUserRepository != null)
                {
                    testUserRepository = null;
                }
            }
        }
        #endregion
    }
}

﻿using IdentityServer.Test.Theory;
using IdentityServer.Controllers;
using IdentityServer.Options;
using IdentityServer.Repositories;
using Microsoft.Extensions.Options;
using Moq;
using System;
using IdentityServer.Infrastructure.Repositories.Interfaces;
using IdentityServer.Infrastructure.Repositories;
using IdentityServer.Services;
using Microsoft.Extensions.Logging;
using IdentityServer4.Services;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Security.Principal;
using Microsoft.AspNetCore.Routing;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace IdentityServer.Test.Fixture
{
    public class ControllerFixture : IDisposable
    {
        private UserRepository userRepository { get; set; }
        private UserClaimRepository userClaimRepository { get; set; }

        public UserController userController { get; private set; }

        public AccountController accountController { get; private set; }

        public UserRepository testUserRepository;
        public ApiResourceRepository testApiResourceRepository;
        public ClientRepository testClientRepository;
        public IdentityResourceRepository testIdentityResourceRepository;
        public PersistedGrantRepository testPersistedGrantRepository;
        public ResourceRepository testResourceRepository;
        public UserClaimRepository testUserClaimRepository;


        public LoginService testLoginService;
        public ClientService testClientService;
        public PersistedGrantService testPersistedGrantService;
        public ProfileService testProfileService;
        public ResourceOwnerPasswordValidatorService testROPasswordValidatorService;
        public ResourceService testResourceService;
        public ILogger<ProfileService> testLogProfileService;
        public IIdentityServerInteractionService identityInteractionService;
        public IAuthenticationSchemeProvider authenticationSchemeProvider;
        public IEventService eventService;

        public ControllerFixture()
        {
            #region Create mock/memory database

            ConfigurationOptions app = new ConfigurationOptions() { MongoConnection = "mongodb://localhost:27017", MongoDatabaseName = "identity4dbController" };
            var mock = new Mock<IOptions<ConfigurationOptions>>();
            mock.Setup(ap => ap.Value).Returns(app);

            userRepository = new UserRepository(mock.Object);
            userClaimRepository = new UserClaimRepository(mock.Object);
            testUserRepository = new UserRepository(mock.Object);
            testApiResourceRepository = new ApiResourceRepository(mock.Object);
            testClientRepository = new ClientRepository(mock.Object);
            testIdentityResourceRepository = new IdentityResourceRepository(mock.Object);
            testPersistedGrantRepository = new PersistedGrantRepository(mock.Object);
            testResourceRepository = new ResourceRepository(mock.Object);
            testUserClaimRepository = new UserClaimRepository(mock.Object);

            testLoginService = new LoginService(testUserRepository);
            testClientService = new ClientService(testClientRepository);
            testPersistedGrantService = new PersistedGrantService(testPersistedGrantRepository);
            testProfileService = new ProfileService(testUserRepository, testLogProfileService, testUserClaimRepository);
            testROPasswordValidatorService = new ResourceOwnerPasswordValidatorService(testLoginService);
            testResourceService = new ResourceService(testResourceRepository, testApiResourceRepository, testIdentityResourceRepository);

            var mockInteractService = new Mock<IIdentityServerInteractionService>();
            var mockAuthSchemeProvider = new Mock<IAuthenticationSchemeProvider>();
            var mockEventService = new Mock<IEventService>();

            var authServiceMock = new Mock<IAuthenticationService>();
            authServiceMock
                .Setup(_ => _.SignInAsync(It.IsAny<HttpContext>(), It.IsAny<string>(), It.IsAny<ClaimsPrincipal>(), It.IsAny<AuthenticationProperties>()))
                .Returns(Task.FromResult((object)null));

            var serviceProviderMock = new Mock<IServiceProvider>();
            serviceProviderMock
                .Setup(_ => _.GetService(typeof(IAuthenticationService)))
                .Returns(authServiceMock.Object);

            HttpContext httpContext = new DefaultHttpContext() {
                RequestServices = serviceProviderMock.Object
            };
            httpContext.User = new GenericPrincipal(
                new GenericIdentity("Admin"),
                new string[0]
                );

            // User is logged out
            httpContext.User = new GenericPrincipal(
                new GenericIdentity(String.Empty),
                new string[0]
                );
            var mockContext = new Mock<HttpContext>();
            mockContext.Setup(ap => ap.User).Returns(httpContext.User);

            var mockIUrlHelper = new Mock<IUrlHelper>();
            mockIUrlHelper.Setup(m => m.IsLocalUrl("www.google.com.br")).Returns(false);

            Mock<ITempDataDictionary> mockTempData = new Mock<ITempDataDictionary>();

            // mock data created by https://barisates.github.io/pretend
            userRepository.Add(new UserTheoryData().GetApplicationUser());



           var  _contextMock = new Mock<HttpContext>();

            var _urlHelperMock = new UrlHelper(new ActionContext(_contextMock.Object, new RouteData(), new Microsoft.AspNetCore.Mvc.Abstractions.ActionDescriptor()));


            #endregion

            // Create Controller
            userController = new UserController(userRepository, userClaimRepository);
            accountController = new AccountController(mockInteractService.Object, testClientService, mockAuthSchemeProvider.Object, mockEventService.Object, testUserRepository, testLoginService);
            accountController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext();
            accountController.ControllerContext.HttpContext = httpContext;
            accountController.Url = _urlHelperMock;
            accountController.TempData = mockTempData.Object;
        }

        #region ImplementIDisposableCorrectly
        /** https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063?view=vs-2019 */
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't
        // own unmanaged resources, but leave the other methods
        // exactly as they are.
        ~ControllerFixture()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                userRepository = null;

                userController = null;
            }
        }
        #endregion
    }
}

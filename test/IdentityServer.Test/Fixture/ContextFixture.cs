﻿
using Castle.Core.Logging;
using IdentityServer.Infrastructure.Repositories;
using IdentityServer.Options;
using IdentityServer.Repositories;
using IdentityServer.Services;
using IdentityServer.Test.Theory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;

namespace IdentityServer.Test.Fixture
{
    public class ContextFixture : IDisposable
    {
        public UserRepository testUserRepository;
        public ApiResourceRepository testApiResourceRepository;
        public ClientRepository testClientRepository;
        public IdentityResourceRepository testIdentityResourceRepository;
        public PersistedGrantRepository testPersistedGrantRepository;
        public ResourceRepository testResourceRepository;
        public UserClaimRepository testUserClaimRepository;


        public LoginService testLoginService;
        public ClientService testClientService;
        public PersistedGrantService testPersistedGrantService;
        public ProfileService testProfileService;
        public ResourceOwnerPasswordValidatorService testROPasswordValidatorService;
        public ResourceService testResourceService;
        public ILogger<ProfileService> testLogProfileService;


        public ContextFixture()
        {
            ConfigurationOptions app = new ConfigurationOptions() { MongoConnection = "mongodb://localhost:27017", MongoDatabaseName="IdentityServerContext" };
            var mock = new Mock<IOptions<ConfigurationOptions>>();
            // We need to set the Value of IOptions to be the SampleOptions Class
            mock.Setup(ap => ap.Value).Returns(app);

            testUserRepository = new UserRepository(mock.Object);
            testApiResourceRepository = new ApiResourceRepository(mock.Object);
            testClientRepository = new ClientRepository(mock.Object);
            testIdentityResourceRepository = new IdentityResourceRepository(mock.Object);
            testPersistedGrantRepository = new PersistedGrantRepository(mock.Object);
            testResourceRepository = new ResourceRepository(mock.Object);
            testUserClaimRepository = new UserClaimRepository(mock.Object);

            testLoginService = new LoginService(testUserRepository);
            testClientService = new ClientService(testClientRepository);
            testPersistedGrantService = new PersistedGrantService(testPersistedGrantRepository);
            testProfileService = new ProfileService(testUserRepository, testLogProfileService, testUserClaimRepository);
            testROPasswordValidatorService = new ResourceOwnerPasswordValidatorService(testLoginService);
            testResourceService = new ResourceService(testResourceRepository, testApiResourceRepository, testIdentityResourceRepository);

            // mock data created by https://barisates.github.io/pretend
            testUserRepository.Add(new UserTheoryData().GetApplicationUser());
        }

        // https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063?view=vs-2019
        #region ImplementIDisposableCorrectly
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't
        // own unmanaged resources, but leave the other methods
        // exactly as they are.
        ~ContextFixture()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (testUserRepository != null)
                {
                    testUserRepository = null;
                }
            }
        }
        #endregion
    }
}

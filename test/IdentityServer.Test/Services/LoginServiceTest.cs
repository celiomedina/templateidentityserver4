﻿using IdentityServer.Services;
using IdentityServer.Test.Fixture;
using IdentityServer.Test.Theory;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace IdentityServer.Test.Services
{
    public class LoginServiceTest : IClassFixture<ServiceFixture>
    {
        LoginService testService;
        public LoginServiceTest(ServiceFixture fixture)
        {
            testService = fixture.testLoginService;
        }

        [Fact]
        public void Login_Test_Ok()
        {
            var result = testService.ValidateCredentials(new UserTheoryData().GetApplicationUser(), "1234");

            Assert.True(result.Exception == null);
        }
    }
}

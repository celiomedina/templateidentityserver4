using IdentityServer.Controllers;
using IdentityServer.Models;
using IdentityServer.Test.Fixture;
using IdentityServer.Test.Theory;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace IdentityServer.Test
{
    // OneTimeSetup 
    /** https://xunit.net/docs/shared-context */
    public class AccountControllerTest : IClassFixture<ControllerFixture>
    {
        AccountController accountController;
        public AccountControllerTest(ControllerFixture fixture)
        {
             accountController = fixture.accountController;
        }

        [Fact]
        public void Account_Login_Ok_Test()
        {
            var result = accountController.Login(
                new LoginInputModel() 
                { 
                    Username = "Admin", 
                    Password = "1234",
                    RememberLogin = true,
                    ReturnUrl = ""
                }, "login");

            Assert.True(result.Id > 0);

            Assert.IsType<RedirectResult>(result.Result);
            Assert.True(result.Exception == null);
            Assert.True(!string.IsNullOrEmpty(((RedirectResult)result.Result).Url));
        }

        [Fact]
        public void Account_Login_Error_Test()
        {
            var result = accountController.Login(
                new LoginInputModel()
                {
                    Username = "Admin",
                    Password = "1234",
                    RememberLogin = true,
                    ReturnUrl = "www.google.com.br"
                }, "login");

            Assert.True(result.Exception != null);
        }


        [Fact]
        public void Account_Logout_Test()
        {
            var result = accountController.Logout(It.IsAny<string>());

            Assert.True(result.Exception == null);
            Assert.True(result.Result != null);
        }
    }
}

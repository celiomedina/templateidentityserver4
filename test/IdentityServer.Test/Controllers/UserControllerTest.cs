using IdentityServer.Controllers;
using IdentityServer.Test.Fixture;
using IdentityServer.Test.Theory;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace IdentityServer.Test
{
    // OneTimeSetup 
    /** https://xunit.net/docs/shared-context */
    public class UserControllerTest : IClassFixture<ControllerFixture>
    {
        UserController userController;
        public UserControllerTest(ControllerFixture fixture)
        {
             userController = fixture.userController;
        }

        [Fact]
        public void User_Create_Ok_Test()
        {
            var result = userController.Create(new UserTheoryData().GetUserRegisterModel()).Result as OkResult;

            Assert.Equal(200, result.StatusCode);
        }

        [Theory]
        [InlineData(0)]
        public void GetUser_WithNonUser_ThenBadRequest_Test(int id)
        {
            var result = userController.Create(null).Result as BadRequestResult;

            Assert.Equal(400, result.StatusCode);
        }
    }
}
